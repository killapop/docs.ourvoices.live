module.exports = {
  title: "ourvoices.live BBB documentation",
  tagline: "A guide to a happy and problem free BBB experience",
  url: "https://docs.ourvoices.live",
  baseUrl: "/",
  onBrokenLinks: "ignore",
  favicon: "img/favicon.ico",
  organizationName: "OurVoices.live", // Usually your GitHub org/user name.
  projectName: "docs.ourvoices.live", // Usually your repo name.
  themeConfig: {
    sidebarCollapsible: false,
    navbar: {
      hideOnScroll: true,
      title: "ourvoices.live BBB guide",
      logo: {
        alt: "ourvoices.live BBB guide",
        src: "img/logo.svg",
      },
      items: [
        {
          to: "docs/",
          activeBasePath: "docs",
          label: "Docs",
          position: "left",
        },
        {
          to: "/img/ourvoices-bbb.pdf",
          label: "download pdf",
          position: "left",
          target: "_blank",
        },
        {
          href: "https://ourvoices.live",
          label: "ourvoices.live BBB",
          position: "right",
        },
      ],
    },
    footer: {},
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          // It is recommended to set document id as docs home page (`docs/` path).
          homePageId: "generalInfo",
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
};
