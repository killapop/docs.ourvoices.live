---
id: preparing
title: "Preparing for your Meeting"
---

## Performance Tips

:::info
The quality of your call depends on several factors like your connectivity, the device you use and what you do on the side during your call. Here are some recommendations.
:::

### Internet Connection

:::note
You need a stable (and fast) internet connection.
:::

- If possible use a network cable to connect your computer to your router `Wired is better than WIFI`
- Turn off any streaming services like movies or music `eg: netflix, youtube or spotify`
- Turn off any syncing applications `eg: nextcloud or dropbox`
- Keep only the browser open and shut down any other apps that you do not need during the call
- If you share your internet connection please try an book a slot when no one else is streaming media or doing anything that uses up the shared bandwidth.
- Sometimes mobile data `3G/4G` works better than WIFI or if you do not have a broadband connection, try using your mobile phone as a hotspot and tether your computer to it.

### Devices

:::note
Use a computer instead of a mobile phone (if you can)
:::

- Use a computer to ensure better participation and allow you better overview of the various interface components which react much better to clicks than finger touches `eg: shared notes, chat, whiteboard and polls etc.,`.
- **[Mobile users]** - Note: keep your screen alive. As BBB runs in the browser only it is normal that your phone screen will time out and go into sleep or locked mode after the set period of inaction. `This will result in you being dropped from the call.`
- Use a good headset or good pair of headphones.
- **[Mobile users]** - Your operating system might not allow the browser use the phone's loudspeaker function so headphones are definitely needed `OR you might have to hold the phone to your ear`.
- Use a wired microphone instead of a bluetooth or wireless one.

### Browsers

:::note
BBB generally works in any browsers. `recommended: Chrome/Chromium, Firefox, Safari`
:::

- Shut down all unused browser tabs
- Switch to another browser if you have trouble connecting
- Some privacy focussed extensions and add-ons might restrict you from joining a call `whitelist your BBB server or turn off the extension/add-on for the call`
