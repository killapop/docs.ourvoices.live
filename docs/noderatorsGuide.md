---
id: moderatorsGuide
title: "Guide for Moderators"
---

:::note

- ourvoices.live uses `greenlight` to manage users and rooms.
- Moderators are user account holders in the `greenlight` front-end for BBB.
- Registration on ourvoices.live is by invitation only.

Please watch this video guide to using greenlight https://www.youtube.com/watch?v=Hso8yLzkqj8&feature=youtu.be

:::

## Your Home Page

- Once logged in you will be brought to your home page where you can create and manage rooms.

![Moderator's Home Page](/img/moderator_home.png)

- Every user has a **Home Room** by default and can create more rooms for sessions.
- From the navigation menu on the top right of the page:
  - Access your peronal settings, change passwords etc by clicking on your avatar.
  - The **Recordings** link will list the recordings that you made of sessions.
  - The **Home** link will bring you back to this page

## Security concerns

:::warning

- URL's to rooms never change. If security is a concern and to avoid being `bombed` or any other unpleasant situations, it is recommended that you take some of the following recommended precautions

:::

- Avoid using your **Home room** for public meetings. Instead create another room for it.
- If possible create a room for a meeting and destroy it afterwards.
- If possible do one of more of the following: ([see room settings](#room-settings))
  - set an access code and share it shortly before the meeting
  - turn on moderator approval for the room
- Avoid sharing a room URL too much in advance.
- For rooms that you do not delete, change the access code frequently.

## Create a room

- Click the `Create a Room` button

![Create a room](/img/moderator_create_room.png)

- You will be prompted to set some properties for the room like a room name, access code etc.,
- You can change these settings at any time.

![Room settings](/img/moderator_room_settings.png)

## Manage Rooms

- click the three dots on a room panel to access some management setting for the room.

![Manage room](/img/moderator_room_settings_0.png)

### Room settings

![Room settings](/img/moderator_room_settings_1.png)

- Set a room name
- Set, Change or Remove the access code for the room
- **Require moderator approval before joining:** If you enable this, when invitees join a meeting they will be held in a waiting room until a moderator approves them. This can be quite problematic for meetings with many participants as it can get quite busy at the start of the meeting as users join the call.
- **Allow any user to start the meeting:**
  - Enable this if you want any invitee who has the URL to the meeting room to start the meeting.
  - Normally a moderator "Starts" a meeting. If invitees open the link to the meeting they will land on a page telling them the meeting hasn't started yet. They will have to manually refresh the page after the meeting is started or reopen the link to join the call.
- **All users join as moderators:** If you enable this, all users in the meeting get full access to the moderator privileges. Please use this with caution and only if you know and trust all the users who are joining your call.

### Share room access

![Room settings](/img/moderator_room_settings_2.png)

- Use this setting to share the room with other registered users in greenlight. This will add the room to their room list and give them full management rights on the room including rights to manage access, change settings and to start the meeting.
- Users that you share access with are automatically also Moderators in the room.
- This is useful to have 2 or more people jointly facilitate meetings `eg: one speaker and one person managing users, breakout rooms etc.,`

## Share a meeting with participants

- On your home page click on the room that you wish to share.
- Check the room settings before you share the URL to the room.
- Remember to note down or copy the access code (if you set one) to share with your invitees.
- Click the copy button to copy the URL to the meeting. Share this and the access code with your invitees.

## Starting the meeting

- Click **Start** to start the meeting.
- It is advised you start the meeting a few minutes before the scheduled start to set things up before people arrive.
- Upload the presentation(s) you wish to use in the meeting.
- Enter agenda or any other notes you might want to into the shared notes.
- If you enabled **Moderator approval required**, watch the sidebar for people arriving in the waiting area, awaiting your approval.

## Moderating the meeting.

- As moderator you have special privileges.
- Click the `gear` icon on the User list to manage several settings in the room.

![Moderating a meeting](/img/moderator_users_0.png)

### Breakout rooms

:::note

See here: [**In a Meeting Room** > **Breakout Rooms**](/docs/userGuide#breakout-rooms)

:::

### Recording a meeting

:::note

See here: [**Recordings** > **Recording Meetings**](/docs/recordings#recording-meetings)

:::

## Ending the meeting

:::warning

- When you end a meeting everthing from the meeting except recordings will be lost.
- Please make note of personal information that might be in the elements that you export/save
- Inform participants that this information is being saved offline

:::

### Export Shared Notes

- If you need to save the shared notes, you can export them.
- Click the `download` icon on the right hand side of the shared notes toolbar.

![Shared notes export](/img/moderator_export_shared_notes.png)

- Choose an export format and save the file.

### Save Public Chat

- If you need to save the public chat. Click on the 3 dots on the top right of the public chat channel.
- Click `save` to save to text file or `copy` to copy it to your clipboard.

![Save Public Chat](/img/moderator_save_chat.png)

:::note

Some information saved in this process might be sensitive `eg: user names and time stamps`

:::

### Logout

- Click the three dots on the top right of the main screen area and choose `Logout` to log out of the meeting.

![Logout](/img/moderator_logout.png)

- This will keep the room open for the others and you will return to your home screen

### End Meeting

- Click the three dots on the top right of the main screen area and choose `End Meeting` to end the meeting.

![End the meeting](/img/moderator_end_meeting.png)

- You will be prompted to confirm the end of the meeting. Click `Yes` to confirm. You will return to your home screen.

![Confirm end](/img/moderator_confirm_end.png)
