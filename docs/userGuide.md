---
id: userGuide
title: "In a BBB Meeting"
---

:::note
We highly recommend you to watch both videos below to get familiar with capabilities of BigBlueButton.

- **General settings:** https://www.youtube.com/watch?v=Hso8yLzkqj8 (4:31)

- **Video for participants:** https://www.youtube.com/watch?v=uYYnryIM0Uw (5:24)

:::

## Joining a meeting

- To use BBB you won’t need an app, everything runs out of the browser.
- Use the browser of your choice (it should work with any).
- **Open the link** that was shared with you using your web browser.
- **Access code:** If the room is protected with a access code you will be prompted to enter the code.

![Acces code screen](/img/access_code.png)

- **User name:** Enter your preferred username

![User name](/img/user_name.png)

> We know there might some hiccups with some browsers, especially on phones (in general when a phone goes to “sleep” the browser usually stops working), because each Android manufacturer deals with web browsing differently (iPhone should work fine). So, if you encounter problems, please be ready to re-enter the session with another browser.

- When you join your browser will also ask you to **enable microphone** and the echo test will be initiated.
- You will be able to hear yourself if you have your microphone connected and switched on. If you do hear yourself, confirm by clicking on **Yes** button (no one else can hear you yet)

  ![Echo test](/img/sA5i5H.png)

- If you don’t hear yourself select **No** and you will be able to change your audio settings.

  > The actual audio and video settings dialog windows differ based on your device, operating system and browser. So carefully read the information or questions displayed in the windows that pop up.

- You will now be in the call room.

:::note
You can use headsets but some operating systems might have “drivers” issues, so be ready to use another set of headsets or the internal audio/microphone of your computer.
:::

:::warning
Remember never to hit the **BACK** button on the browser, or you will leave the meeting!
:::

## The Meeting room

The meeting room main screen is composed of different interactive elements.

![Meeting Room](/img/meeting_room.png)

1. **Main Screen area:** where webcam streams, presentations and screenshares are displayed.
2. [**User list**](#user-list) shows all users currently in the meeting. **Note:** the user's current status (unmuted, muted, hand raised etc.,) displayed on their icon.
3. [**Chat and Shared Notes**](#chat-and-shared-notes).
   - **Shared notes** is a scaled down etherpad for collaborative note taking
   - **Chats** can be public or private (click a username to start a private chat with them)
4. [**Audio and Camera Toggle**](#mute-unmute-and-share-your-camera-and-screen) your Microphone, Audio, Webcam and Screenshare respectively
5. [**Room Settings**](#room-settings) Open the room settings for yourself. here you can set data saving settings to improve the quality of your call.
6. **Presentation / Whiteboard Toggle** launches the currently uploaded presentation / whiteboard.
7. **Speaker List:** Shows all users with microphones on. The current speaker is in a darker blue and you in purple.
8. [**Presenter Actions:**](#presenters) As presenter you can upload presentations, start polls etc.

## Room Settings

- On the 3 dots menu on the top right you can configure some of your personal options.

![Launch the meeting settings](/img/meeting_settings_0.png)

- Use settings for personal alerts, language and font size.

![Set your meeting preferences](/img/meeting_settings_1.png)

- Use data savings if your connection is not good, to reduce bandwidth load.

![Optimisations for data saving](/img/meeting_settings_2.png)

## Mute, Unmute and Share your Camera and Screen

- Use the buttons to toggle your microphone, sound, camera and screensharing (presenters only).

![Toggle buttons](/img/toggle_buttons.png)

:::note
Depending on your device, operating system and browser you might be prompted to confirm or choose preferred devices (audio source, mic, camera, window to share, etc.,)
:::

## Chat and Shared notes

### Chats

All meeting rooms have a shared chat where all participants can type messages.

You can also privately chat one-on-one with other participants. Click on a user's name from the user list to launch a private chat with them.

### Shared Notes

Shared notes are a scaled down etherpad which allows real time collaborative editing and is ideal for taking notes of the meeting.

Moderators can also paste the agenda of the meeting in here before all participants join.

## User List

- You can see a list of users participating in the call on the left hand side of the screen.
- Your own name will be the first.
- Click on it to set your status, which is your icon. This can indicate how you feel about the meeting , and you can also click on **raise hand** if you want to indicate that you wish to speak.

![Change your status](/img/user_interactions.png)

- If you click on somebody else’s name, you can initiate a private chat (a chat no one else can see).
- If you are a meeting administrator you can see other features to manage participants, ie to make someone presenter, unmute, etc

## Presenters

:::note
There can be only one presenter in a meeting at any point.
:::

- At the start of a meeting, the room creator is the default presenter.
- Presenters can:
  - Upload a presentation.
  - Start a poll
  - Enable the whiteboard and allow/revoke public editing permissions.
  - Make another user presenter.
- You become the presenter when the moderator or the previous presenter makes your one.

:::info
Please see here - https://support.blindsidenetworks.com/hc/en-us/articles/360024689292-Use-the-presentation-controls for more detailed instructions on presenter features and actions
:::

### Presentations

#### Upload

- To upload a presentation you have to have the Presenter role, either assigned to you or taken by you. To start first click on the big blue `+` button and then select Upload a presentation.

![Presenter actions](/img/presenter_actions.png)

- A window where you can upload your presentation will open. It already has the default presentation on the top of the list and you can add new ones by dropping them on the marked field or selecting them by clicking or browse for files.

:::note

- The uploaded presentation will immediately appear in the room (so if you don’t want to show it now, load beforehand or include an empty slide).
- All presenters can see and use the presentations loaded by others presenters as the presenter role is passed between participants

:::

- After you select the file, it appears on the list and you need to confirm the upload by clicking on the Upload button

![Upload a Presentation](/img/upload_presentation_0.png)

- If you would like to select or upload another presentation go to + button again and select Upload a presentation. The current presentation has a label CURRENT and a green circle checkbox. To select another presentation click in the empty circle and click button Confirm.

#### Share

- If you would like the users to be able to download the presentation then enable sharing by clicking on the icon next to green circle checkbox.

![Share a presentation](/img/share_presentation.png)

- The users will now have a little download icon available in the bottom left corner of the presentation.

#### Delete

- To remove a presentation click on trash can icon.

![Delete a presentation](/img/delete_presentation.png)

#### Presentation tools

- You will be able to use additional features to write and draw over the presentation.
- To access the available tools by clicking on the hand icon on the right hand side of presentation in the main screen.

![Presentation tools](/img/presentation_tools.png)

- You can also make these tools available to other participants. Click on the multi-user whiteboard icon at the bottom to enable it.

![Share presentation tools](/img/presentation_tools_0.png)

### Screenshares

- As a moderator or presenter you can also share (or un-share) the screen. Click on the **Share your screen** icon below the presentation.

![Screenshare Toggle](/img/screensharing_toggle.png)

- A browser notification will pop up to allow you to select the screen you would like to share (the whole window or a specific application or a browser tab). Make your choice and confirm with the **Allow** button.

![Screenshare Allow](/img/screensharing_allow.png)

### Polls

- As a presenter you can create a poll. To create a poll click on the big blue `+` button and select **Start a poll**.

![Start a Poll](/img/poll_start.png)

- This will open up a window in the Chats/Shared notes panel where you can set up your poll.
- You can select one of the predefined polls or create a custom one.

![Setup Polls](/img/poll_setup.png)

### Breakout rooms

Breakout rooms can be created by clicking on the gear icon at the top of the **Users list** and selecting **Create breakout rooms**.

#### Create

:::note
When you start to manage breakout rooms you cannot carry out any other actions or see anything of the meeting room until you finalize and/or exit breakout rooms management.
:::

![Create Breakout](/img/breakout_start.png)

- Select the number of rooms you would like to create (between 2 and 8).
- Assign users to rooms by:
  - dragging and dropping users' names to rooms
  - Randomly assigning them to rooms. OR
  - Allowing users to decide which room to joining
- You will also need to set the duration of breakout sessions.
- Confirm the settings and launch the breakout rooms by clicking on **Create** button.

:::note
You will not be able to alter the duration of the breakouts so please be careful while setting it up.
:::

![Setup Breakouts](/img/breakout_setup.png)

#### Join

- Meeting participants will get be prompted to confirm and join the breakout room assigned or asked to choose one to join based on the method of assigning chosen above.

![Join a Breakout room](/img/breakout_join.png)

- The Breakout room launch in a new browser tab.
- When participants join the breakout room they will see a notification about remaining time on top of the window.

![Breakout timer](/img/breakout_timer.png)

:::note

- Breakout rooms are full featured meeting rooms.
- Chats, Notes, Pads and other interactions are as they are in the main meeting room.

:::

- The participant will continue to be present main meeting room, in the original browser tab, just without sound. All features will return to defaults when they return from the breakout rooms.
- Participants will also see the last content on the screen a their main breakout room screen (for example if it was a presentation, they will see the last slide only).
- The first participant to join will have the presenter role. Recording and chats on the rooms are separate from the main room and from each other.

#### Manage

- The presenter who created the rooms will remain in the main meeting room and will can see an overview of remaining time and the spread of participants in the breakout rooms.
- All meeting moderators can move between rooms, joining with **audio only**.
- The creator of the breakout rooms can manage them as well.

:::warning
Remember to save notes and chats before ending the breakout room. They will be lost when the room is closed.
:::

## Ending a Meeting

- To end the meeting you can simply close the browser’s tab or window.
- If you are the moderator end the meeting by opening the menu in the top right corner of the room and selecting **End meeting**. [see **Guide for Moderators** > **Ending the Meeting**](/docs/moderatorsGuide#ending-the-meeting)

![End a Meeting](/img/meeting_end.png)
