---
id: recordings
title: "Recordings"
---

## General notes

## Security Concerns

## Managing Recordings

## Recording Meetings

- Only moderators will see a button `Start recording` at the top of the main screen area.

![Start Recording](/img/recording_start.png)

:::note

Please inform participants that the meeting is being recorded.

:::

- Click the `Start Recording` button to record the meeting.
- Click `Yes` in the confirmation popup window to start the recording.

![Start Recording](/img/recording_start_0.png)

-
