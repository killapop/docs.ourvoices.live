---
id: generalInfo
title: "General Info"
---

:::note
This guide is a compilation of tips, recommendations and things to watch out for to get the best out of your BBB experience.

- Also [see here for video tutorials for using BBB](https://bigbluebutton.org/html5/)

:::

## What you need

- An internet connection
- A connected device (Mobile or Computer).
- A browser on your device `BBB connects via a browser like any other website`
- **You do not need an app or any other software to be installed.**

## Quality and Performance

The quality of your BBB session depends upon:

- The speed and stability of your internet connection
- The device you use and it's capacity
- The security and permissions settings on your device
- The security and permissions setting in your browser and it's add-ons
