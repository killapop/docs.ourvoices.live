module.exports = {
  docusaurus: {
    "BBB Users": ["generalInfo", "preparing", "userGuide"],
    Moderators: ["moderatorsGuide"],
    Recordings: ["recordings"],
  },
};
